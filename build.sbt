name := "CapsifiCodingTests"

version := "0.1"

scalaVersion := "2.11.12"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.8",
  "org.specs2" %% "specs2-core" % "4.2.0" % "test"
)