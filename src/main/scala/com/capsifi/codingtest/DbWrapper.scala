package com.capsifi.codingtest

import java.util.UUID

import scala.concurrent.{Await, Future, TimeoutException}
import scala.concurrent.duration._
import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.control.NonFatal

class DbWrapper {

  //
  // This class represents an imaginary call to a database on a remote server.
  // The function getNextUUID() calls the remote database and retrieves an available
  // UUID from the server (or None if the call fails).
  //
  // In production use, this call has been found to take up to 10 seconds to process.
  // This is causing a bad user experience.
  //
  // Modify getNextUUID() so that if makeHostCall() does not return within 2 seconds,
  // getNextUUID() returns None. The customer has agreed that this is an acceptable short-term
  // solution to the performance problem. Do not change the signature for getNextUUID.
  //

  private val DB_TIMEOUT = 2.seconds

  def getNextUUID(): Option[String] = {

    //
    // This is the call that is taking too long:
    //
    try {
      Await.result(Future(makeHostCall), DB_TIMEOUT + 1.millis)
    } catch {
      case _: TimeoutException =>
        // log warn message here
        None
      case NonFatal(ex) =>
        // log error message here
        throw ex
    }
  }

  private[codingtest] def makeHostCall: Option[String] = {
    //
    // Pretend that this is a host call that may take up to 10 seconds, and returns meaningful data
    //
    // DO NOT MODIFY THIS FUNCTION
    //
    Thread.sleep(Random.nextInt(10000))
    Some( UUID.randomUUID().toString )
  }
}

