package com.capsifi.codingtest

object DbWrapperTest {

  def main(args: Array[String]): Unit = {

    val db = new DbWrapper
    for( i <- 0 until 10 ) {

      val start = System.nanoTime()
      val res = db.getNextUUID()
      val end = System.nanoTime()
      val durationMs = (end - start) / 1000000L

      val ok = if( durationMs > 2000 ) {
        res.isEmpty
      } else {
        res.isDefined
      }

      println( s"$i: $ok in $durationMs ms")

    }

  }
}

