package com.capsifi.codingtest

import java.util.UUID
import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor.{ActorSystem, Props}
import com.capsifi.codingtest.actors.DbCacheActor
import com.capsifi.codingtest.actors.DbCacheActorMessage.{UPDATE, UPDATE_FUTURE}

import scala.concurrent.duration._
import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global

class DbWrapperV2()(implicit system: ActorSystem) {

  private val UUIDCache = new ConcurrentLinkedQueue[String]()

  private val MAX_CACHE_SIZE = 100
  private val ATTEMPT_DELAY = 100.millis

  private val cache = system.actorOf(
    Props(new DbCacheActor(UUIDCache, MAX_CACHE_SIZE)(makeHostCall)),
    "db-cache-actor"
  )

  // schedule uuid fetching
  system.scheduler.schedule(50.millis, 100.millis, cache, UPDATE)
  //system.scheduler.schedule(50.millis, 200.millis, cache, UPDATE_FUTURE)

  def getNextUUID(): Option[String] = {

    def tryGetNextUUID(): Option[String] = {
      val uuid = UUIDCache.poll()
      if (uuid == null) {
        Thread.sleep(ATTEMPT_DELAY.toMillis)
        tryGetNextUUID()
      } else {
        Some(uuid)
      }
    }

    val uuid = tryGetNextUUID()
    uuid
  }

  private def makeHostCall: Option[String] = {
    //
    // Pretend that this is a host call that may take up to 10 seconds, and returns meaningful data
    //
    // DO NOT MODIFY THIS FUNCTION
    //
    Thread.sleep(Random.nextInt(8000) + 2000)
    Some( UUID.randomUUID().toString )
  }

}
