package com.capsifi.codingtest

import akka.actor.ActorSystem

object DbWrapperV2Test {

  def main(args: Array[String]): Unit = {

    implicit val actorSystem = ActorSystem("db-wrapper-cache")

    val db = new DbWrapperV2
    for( i <- 0 until 100 ) {

      val start = System.nanoTime()
      val res = db.getNextUUID()
      val end = System.nanoTime()
      val durationMs = (end - start) / 1000000L

      val ok = if( durationMs > 2000 ) {
        res.isEmpty
      } else {
        res.isDefined
      }

      println( s"$i: $ok in $durationMs ms")

    }

    actorSystem.shutdown()
  }
}

