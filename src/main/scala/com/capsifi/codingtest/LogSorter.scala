package com.capsifi.codingtest

import com.capsifi.codingtest.models.LogMessage
import com.capsifi.codingtest.models.LogLevelEnum
import com.capsifi.codingtest.models.LogLevelUtils._

class LogSorter {

  //
  // Given a sequence of lines from an example log file in the following format:
  //
  // [System ID] [Date & Time] [Log Level] [Message]
  //
  // Each item is separated by a single space, and there are no spaces within the System ID,
  // Date & Time, and Log Level. Date & Time is provided in the format
  //
  //   YYYY-MM-DDTHH:MI:SS.MMM
  //
  // All times are assumed to be in UTC.
  //
  // Implement a function to sort the lines so that lines with a log level of FATAL appear
  // first in the file, sorted by Date & Time, and then all other lines sorted by Date & Time.
  // Lines sorted by Date should be from earliest to latest.
  //
  // e.g. The following three lines:
  //
  //    WEB1 2018-06-20T10:11:22.000 INFO System start
  //    WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file
  //    WEB1 2018-06-20T09:50:58.000 INFO System shutdown
  //
  // Should be sorted as:
  //
  //    WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file       - FATAL lines appear first
  //    WEB1 2018-06-20T09:50:58.000 INFO System shutdown                   - Other lines sorted by Date & Time
  //    WEB1 2018-06-20T10:11:22.000 INFO System start
  //
  // Implement this in the function sortLines() below. Do not modify the function signature,
  // but you may create any additional functions or classes you like.
  //

  def sortLines( logfile: Seq[String] ): Seq[String] = {

    // Implement your solution here
    logfile
      .map(_.split(" ", 4).toList)
      .map(d => LogMessage(d(0), d(1), d(2).toLogLevel.getOrElse(LogLevelEnum.UNKNOWN), d(3)))
      .sortBy(d => (d.logLevel.id, d.dateTime))
      .map(_.toString)
  }
}
