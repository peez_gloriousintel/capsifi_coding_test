package com.capsifi.codingtest

object LogSorterTest {

  def main(args: Array[String]): Unit = {

    val ls = new LogSorter

    var successes = 0
    var failures = 0

    for( (c, i) <- cases.zipWithIndex ) {

      println(s"\nTesting case $i:")

      val res = ls.sortLines( c._1 )
      var correct = true
      for( l <- res.zip( c._2 )) {
        if( l._1 != l._2 ) {
          println( s"MISMATCH: $l" )
          correct = false
        }
      }

      correct match {
        case true =>
          successes += 1
          println("OK")
        case false => failures += 1
      }
    }

    println( s"All done: $successes successful, $failures failed")
  }

  def cases:Seq[(Seq[String], Seq[String])] = Seq(
    // Input Test 1 - basic case
    (
      Seq(
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown"
      ),
      Seq(
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB1 2018-06-20T10:11:22.000 INFO System start"
      )
    )
  )

}

