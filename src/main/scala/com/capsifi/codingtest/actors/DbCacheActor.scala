package com.capsifi.codingtest.actors

import java.util.UUID
import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor.{Actor, Props}

import scala.concurrent.ExecutionContext.Implicits.global
import com.capsifi.codingtest.actors.DbCacheActorMessage.{SHUTDOWN, UPDATE, UPDATE_FUTURE}

import scala.concurrent.Future

class DbCacheActor(cache: ConcurrentLinkedQueue[String], maxSize: Int = 100)(fn: => Option[String]) extends Actor {

  override def receive: Receive = {
    case UPDATE => {
      if (cache.size < maxSize) {
        val workerId = UUID.randomUUID().toString
        val child = context.actorOf(
          Props(new DbCacheWorker(cache)(fn)),
          s"db-cache-worker-$workerId"
        )
        child ! UPDATE
      }
    }
    case UPDATE_FUTURE => {
      Future {
        while (cache.size < maxSize) {
          fn.map(cache.add)
          Thread.sleep(50)
        }
      }
    }
    case SHUTDOWN =>
      context.stop(self)
  }

}

class DbCacheWorker(cache: ConcurrentLinkedQueue[String], maxSize: Int = 100)(fn: => Option[String]) extends Actor {

  override def receive: Receive = {
    case UPDATE =>
      fn.map(cache.add); context.stop(self)
  }

}

object DbCacheActorMessage {

  case object UPDATE

  case object UPDATE_FUTURE

  case object SHUTDOWN

}