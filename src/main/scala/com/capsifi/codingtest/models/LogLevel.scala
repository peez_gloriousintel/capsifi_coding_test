package com.capsifi.codingtest.models

import com.capsifi.codingtest.models.LogLevelEnum.LogLevel

object LogLevelEnum extends Enumeration {
  type LogLevel = Value
  val FATAL = Value(1, "FATAL")
  val ERROR = Value(2, "ERROR")
  val WARN = Value(3, "WARN")
  val INFO = Value(4, "INFO")
  val DEBUG = Value(5, "DEBUG")
  val TRACE = Value(6, "TRACE")
  val ALL = Value(7, "ALL")
  val UNKNOWN = Value(8, "UNKNOWN")
}

object LogLevelUtils {
  implicit class StringToLogLevel(s: String) {
    def toLogLevel: Option[LogLevel] = {
      LogLevelEnum.values.find(_.toString == s)
    }
  }
}