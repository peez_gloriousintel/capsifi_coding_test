package com.capsifi.codingtest.models

import com.capsifi.codingtest.models.LogLevelEnum.LogLevel

case class LogMessage(systemId: String,
                      dateTime: String, //NOTE: can be DateTime for more accurate
                      logLevel: LogLevel,
                      message: String
                     ) {
  override def toString: String = s"$systemId $dateTime ${logLevel.toString} $message"
}
