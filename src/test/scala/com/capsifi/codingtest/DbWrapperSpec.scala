package com.capsifi.codingtest

import java.util.UUID

import org.specs2.mutable.Specification

class DbWrapperSpec extends Specification {

  "DbWrapper" should {

    "return non-empty string if getting result before timeout" in {
      val rapidDbWrapper = new DbWrapper {
        override private[codingtest] def makeHostCall = {
          Thread.sleep(200)
          Some( "Fake UUID" )
        }
      }

      rapidDbWrapper.getNextUUID() mustEqual Some("Fake UUID")
    }

    "return empty string if getting result after timeout" in {
      val slowDbWrapper = new DbWrapper {
        override private[codingtest] def makeHostCall = {
          Thread.sleep(2200)
          Some( "Fake UUID" )
        }
      }

      slowDbWrapper.getNextUUID() mustEqual None
    }

  }

}
