package com.capsifi.codingtest

import org.specs2.mutable.Specification

class LogSorterSpec extends Specification {

  "LogSorter" should {

    val logSorter = new LogSorter

    "return sorted log messages correctly" in {
      val input = Seq(
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO System restart"
      )
      val expect = Seq(
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO System restart",
        "WEB1 2018-06-20T10:11:22.000 INFO System start"
      )
      val actual = logSorter.sortLines(input)
      actual mustEqual expect
    }

    "return sorted log messages correctly with different log levels" in {
      val input = Seq(
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T19:50:58.000 ERROR System shutdown unexpectedly",
        "WEB2 2018-06-20T13:40:12.000 DEBUG Receiving request from client",
        "WEB1 2018-06-20T08:24:32.000 ALL System work normally",
        "WEB2 2018-06-20T01:52:46.000 WARN DB Connection is slower than 3000 ms",
        "WEB1 2018-06-20T22:04:08.000 TRACE Processing response from external calls"
      )
      val expect = Seq(
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T19:50:58.000 ERROR System shutdown unexpectedly",
        "WEB2 2018-06-20T01:52:46.000 WARN DB Connection is slower than 3000 ms",
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T13:40:12.000 DEBUG Receiving request from client",
        "WEB1 2018-06-20T22:04:08.000 TRACE Processing response from external calls",
        "WEB1 2018-06-20T08:24:32.000 ALL System work normally"
      )
      val actual = logSorter.sortLines(input)
      actual mustEqual expect
    }

    "return sorted log messages correctly with the same log levels" in {
      val input = Seq(
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 INFO System restart",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO System restart"
      )
      val expect = Seq(
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO System restart",
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 INFO System restart"
      )
      val actual = logSorter.sortLines(input)
      actual mustEqual expect
    }

    "return sorted log messages correctly with missing a message column" in {
      val input = Seq(
        "WEB1 2018-06-20T10:11:22.000 INFO System start",
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO "
      )
      val expect = Seq(
        "WEB2 2018-06-20T10:12:30.000 FATAL Missing configuration file",
        "WEB1 2018-06-20T09:50:58.000 INFO System shutdown",
        "WEB2 2018-06-20T09:58:16.000 INFO ",
        "WEB1 2018-06-20T10:11:22.000 INFO System start"
      )
      val actual = logSorter.sortLines(input)
      actual mustEqual expect
    }

  }

}
