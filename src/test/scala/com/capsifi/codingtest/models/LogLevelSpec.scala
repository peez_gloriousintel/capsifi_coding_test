package com.capsifi.codingtest.models

import org.specs2.mutable.Specification
import com.capsifi.codingtest.models.LogLevelUtils._

class LogLevelSpec extends Specification {

  "LogLevel" should {

    "convert string to log level correctly" in {
      "FATAL".toLogLevel mustEqual Some(LogLevelEnum.FATAL)
      "ERROR".toLogLevel mustEqual Some(LogLevelEnum.ERROR)
      "WARN".toLogLevel mustEqual Some(LogLevelEnum.WARN)
      "INFO".toLogLevel mustEqual Some(LogLevelEnum.INFO)
      "DEBUG".toLogLevel mustEqual Some(LogLevelEnum.DEBUG)
      "TRACE".toLogLevel mustEqual Some(LogLevelEnum.TRACE)
      "ALL".toLogLevel mustEqual Some(LogLevelEnum.ALL)
      "XXX".toLogLevel mustEqual None
    }

  }

}
