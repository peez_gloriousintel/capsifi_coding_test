package com.capsifi.codingtest.models

import org.specs2.mutable.Specification

class LogMessageSpec extends Specification {

  "LogMessage" should {

    "convert message model to string correctly" in {
      val message = LogMessage("WEB1", "2018-06-20T10:11:22.000", LogLevelEnum.INFO, "System start")
      val expect = "WEB1 2018-06-20T10:11:22.000 INFO System start"
      message.toString mustEqual expect
    }

  }

}
